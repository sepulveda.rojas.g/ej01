#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#El ejemplo consiste en crear un código que nos entregue el valor que deberá cobrar a los que utilizan el estacionamiento,
#tomando en cuenta que las fracciones de hora se toman como completas.

#Entradas: Se le pedirá al usuario que ingrese el valor de un estacionamiento por horas, la cantidad de horas que estuvo.
#Procedimiento: Es necesario que si la cantidad de horas no es entera, se redondee para tener un valor exacto.
#Salidas: Se le entregará al usuario la cantidad que deberá pagar por las horas transcurridas.

precio = int (input("Ingrese el precio de estacionamiento por hora: "))
horas = float (input("Ingrese la cantidad de horas transcurridas: "))

valortotal = precio * round(horas)

print ("El valor total que deberá pagar el usuario por", horas,"horas es de $", valortotal, "pesos")