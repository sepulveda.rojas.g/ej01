#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#El ejemplo consiste en calcular la distancia entre dos puntos del plano cartesiano, utilizando la fórmula de la distancia
#y también la del teorema de pitágoras.

#Entradas: Se le pedirá al usuario que ingrese los puntos del plano cartesiano que necesita calcular.
#Procedimiento: Es necesario saber el teorema de pitágoras (hip2 = cat2 + cat2) y la fórmula de la distancia (d2 = (x2 - x1)2 + (y2 - y1)2)
#Salidas: Entregará la distancia final entre ambos puntos en el plano cartesiano.

import math

x1 = float(input("Ingrese el valor de abscisa x1: "))
y1 = float(input("Ingrese el valor de ordenada y1: "))

x2 = float(input("Ingrese el valor de abscisa x2: "))
y2 = float(input("Ingrese el valor de ordenada y2: "))

abscisas = x2 - x1
ordenadas = y2 - y1

distancia = (abscisas * abscisas + ordenadas * ordenadas)

print ("La distancia final es de:", math.sqrt(distancia))