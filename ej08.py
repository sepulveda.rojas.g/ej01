#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#El ejemplo consiste en que se requiere obtener el area de la figura en la forma A de la siguiente imagen. Para resolver este problema se puede 
# partir de que esta formada por tres figuras: dos triangulos rectangulos, con H como hipotenusa y R como uno de los catetos, que tambien es 
# el radio de la otra figura, una semicircunferencia que forma la parte circular (ver forma B).

#Entradas: Se le pedirá al usuario que ingrese los valores de H y R, siguiendo el dibujo de ambas figuras.
#Procedimiento: A través de cálculos matemáticos se sacará el valor del cateto faltante y de paso el área del triángulo y circulo vistos en la imagen.
#Salidas: Se imprimirá el valor final de ambos, siendo este el área total de las figuras.

import math

valor_h = float(input("Ingrese el valor H de la figura: "))

valor_r = float(input("Ingrese el valor R de la figura: "))

cateto = math.sqrt(valor_h * valor_h - valor_r * valor_r)

areatriangulo = 2 * (valor_r * cateto) / 2
areacirculo = (3.14 * (valor_r * valor_r)) / 2

areatotal = areatriangulo + areacirculo

print ("El área total de la figura es de ", areatotal)