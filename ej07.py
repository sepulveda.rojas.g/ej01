#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#“El buen gordito” ofrece hamburguesas sencillas, dobles y triples, las cuales tienen un costo de $2000,
#$2500 y $3000 respectivamente. La empresa acepta tarjetas de crédito con un cargo de 5 % sobre la
#compra. Suponiendo que los clientes adquieren solo un tipo de hamburguesa, realice un algoritmo para
#determinar cuanto debe pagar una persona por N hamburguesas.

#Entradas: Al iniciar, se le pedirá al usuario que ingrese la cantidad de hamburguesas(N) que desea, para así pedirle según la cantidad el
#tipo de cada una de ellas para así ir sumano los valores.

#Procedimiento: Es necesario crear un bucle for, ya que dependiendo la cantidda de hamburguesas que pida el usuario se repetirán las preguntas,
#almacenando los datos para poder saber si es necesario hacer el cargo a la boleta del 5%, según su preferencia de la manera de pagar.

#Salidas: Se le entregará al usario el valor final de su pedido.

Pago = 0
n = int (input ("Ingrese la cantidad de hamburguesas: "))
for i in range (1, n + 1):
    print ("Hamburguesa " + repr(i))
    print ("Seleccione el número del tipo de hamburguesa: ")
    print ("(1) Sencilla")
    print ("(2) Doble")
    print ("(3) Triple")

    tipohamburguesa = 0
    while tipohamburguesa < 1 or tipohamburguesa > 3:
        tipohamburguesa = int (input ("\n"))
        if tipohamburguesa < 1 or tipohamburguesa > 3:
            print ("Valor incorrecto, intente nuevamente.")

    Valor = 0
    if tipohamburguesa == 1:
        Valor = 2000
    if tipohamburguesa == 2:
        Valor = 2500
    if tipohamburguesa == 3:
        Valor = 3000

    print ("\nSeleccione la forma de pago: ")
    print ("(1) Tarjeta")
    print ("(2) Efectivo")
   
    formapago = 0
    while formapago < 1 or formapago > 2:
        formapago = int (input ("\n"))
        if formapago < 1 or formapago > 2:
            print ("Valor incorrecto, intente nuevamente")

    if formapago == 1:
        Cargo = Valor * 0.05
    else:
        Cargo = 0

    Pago = Pago + Valor + Cargo

print ("\nEl valor total a pagar es de", round(Pago) , "pesos")