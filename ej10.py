#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#Una empresa constructora vende terrenos con la forma A de la figura. Se requiere crear un programa para obtener el área 
#respectiva de un terreno de medidas de cualquier valor.

#Entradas: Se le pedirá al usuario que ingrese los valores de cada lado del terreno.

#Procedimiento: Según las cantidades obtenidas realizaremos las operaciones para calcular el valor de los lados de las
#figuras, para calcular el área de cada una de las figuras que lo componen y así entregar el área total del terreno.

#Salidas: Se le entregará el área total que tendrá el terreno, según las dimensiones obtenidas.

lado_a = float(input("Ingrese el valor del lado A del terreno: "))

lado_b = float(input("Ingrese el valor del lado B del terreno: "))

lado_c = float(input("Ingrese el valor del lado C del terreno: "))


lado_a = lado_a - lado_c

area_triangulo = (lado_b * lado_a) / 2
area_rectangulo = lado_b * lado_c

area_total = area_triangulo + area_rectangulo

print("El área tota del terreno a vender corresponde a ", area_total)
