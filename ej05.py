#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#Una empresa que contrata personal requiere determinar la edad de cada una de las personas que solicitan trabajo, pero cuando se les 
#realiza la entrevista solo se les pregunta el año en que nacieron. También se requiere el promedio de edad de las personas contratadas.

#Entradas: Se le pedirá al usuario que ingrese la cantidad de entrevistados.

#Procedimiento: A través de un bucle for podremos hacer la pregunta a la cantidad de entrevistados que desea el usuario, almacenando el año de 
#nacimiento para poder calcular su edad actual y así tener el promedio entre las edades.

#Salidas: Se le entregará al usuario el promedio entre las edades, calculadas previamente.

promedio = 0
añoactual = 2021

entrevistados = int (input ("Ingresa la cantidad de entrevistados: "))

for i in range (1, entrevistados + 1):
    print ("Entrevistado " + repr (i))
    añonacimiento = int (input ("Ingrese el año de nacimiento: "))
    edad = añoactual - añonacimiento
    promedio = promedio + edad

if entrevistados == 0:
    promedio = 0

else:
    promedio = promedio/entrevistados
print ("El promedio entre las edades de los entrevistados es " + repr(promedio))