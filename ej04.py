#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#El ejemplo consiste en que se requiere un programa para determinar cuánto ahorrará una persona en un año, si al final de cada mes deposita 
#variables cantidades de dinero; además, se requiere saber cuánto lleva ahorrado cada mes.

#Entradas: Se le pedirá al usuario que ingrese el monto a ahorrar durante cada mes del año.

#Procedimiento: A través de un bucle while que nos ayudará con el almacenamiento de los datos, y que sea posible entregar el valor mensual y 
#final de sus ahorros.

#Salidas: Cada mes se le imprimirá lo que lleva ahorrado de los meses anteriores, y al final el monto total durante los 12 meses.

x = 1
suma = 0

while x <= 12:

    ahorro = float(input("Ingrese el ahorro del mes: "))

    suma = suma + ahorro

    print ("El ahorro acumulado hasta el mes", x, "es $", round(suma))

    x = x + 1
print ("El ahorro durante el año corresponde a $", round(suma), "pesos")
