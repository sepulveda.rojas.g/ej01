#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#El ejemplo consiste en inicializar una variable en None y si la palabra ingresada es “Hola” debe imprimir Chao, caso 
#contrario debe decir, no entiendo tú mensaje e imprimir el valor de la variable.

#Entradas: Se le pide al usuario que ingrese una palabra.
#Procedimiento: Se analizará si la palabra entregada por el usuario es "Hola" u otra.
#Salidas: Si la palabra es "Hola" se le imprimirá el valor "Chao", mientras que si no cumple con este valor se imprimirá "No entiendo tu mensaje"
 
a = None

palabra = input("Buenod días, ingrese la palabra: ")

if palabra == "Hola" or palabra == "hola":
    print("Chao.")
else:
    print("No entiendo tu mensaje:", palabra)