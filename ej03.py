#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021 

#En el siguiente ejemplo debemos de entregarle al usuario el valor final de sus productos, ya que dependiendo el valor
#actual se le asignará 15% o 8% de descuento.

#Entradas: Se le pide al usuario que ingrese los valores de tres trajes de vestir.
#Procedimiento: Se utilizará una condición para cada caso, dependiendo el valor del traje se le asignará un descuento.
#Salida: Se le entregará el valor final de los trajes, con su descuento respectivo.

compra1 = int (input("Ingrese el valor de su primer traje: "))

if compra1 > 10000:
    descuento = compra1 * 0.15
    finalcompra = compra1 - descuento
    print ("- El valor final de su traje de", compra1, "pesos con descuento es de", round (finalcompra), "pesos")

elif compra1 <= 10000:
    descuento = compra1 * 0.08
    finalcompra = compra1 - descuento
    print ("- El valor final de su traje de", compra1, "pesos con descuento es de", round (finalcompra), "pesos")

else:
    print ("El valor es incorrecto, intente nuevamente")


compra2 = int (input("Ingrese el valor de su segundo traje: "))

if compra2 > 10000:
    descuento = compra2 * 0.15
    finalcompra = compra2 - descuento
    print ("- El valor final de su traje de", compra2, "pesos con descuento es de", round (finalcompra), "pesos")

elif compra2 <= 10000:
    descuento = compra2 * 0.08
    finalcompra = compra2 - descuento
    print ("- El valor final de su traje de", compra2, "pesos con descuento es de", round (finalcompra), "pesos")

else:
    print ("El valor es incorrecto, intente nuevamente")


compra3 = int (input("Ingrese el valor de su tercer traje: "))

if compra3 > 10000:
    descuento = compra3 * 0.15
    finalcompra = compra3 - descuento
    print ("- El valor final de su traje de", compra3, "pesos con descuento es de", round (finalcompra), "pesos")

elif compra3 <= 10000:
    descuento = compra3 * 0.08
    finalcompra = compra3 - descuento
    print ("- El valor final de su traje de", compra3, "pesos con descuento es de", round (finalcompra), "pesos")

else:
    print ("El valor es incorrecto, intente nuevamente")