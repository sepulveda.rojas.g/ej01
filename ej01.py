#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 03 de septiembre del 2021

#El ejemplo consiste en que se debe determinar el valor total de los lápices que ha comprado el usuario, ya que 
# dependiendo la cantidad de unidades compradas el valor unitario cambia.

#Entradas: Se le pedirá al usuario que ingrese una cantidad válida de lápices

#Procedimiento: Si la cantidad de lápices es igual o sobrepasa los 1000 se le asignará un valor de $85 c/u, mientras que
#si es un número menor a ese, cada uno costará $90.

#Salidas: Dependiendo la cantidad de lápices, se le entregará el valor total a pagar.

cantidadlapices = int (input ("Buenos días, ingrese la cantidad de lápices que desea: "))

if cantidadlapices >= 1000:
    cantidadtotal = cantidadlapices * 85
    print ("EL valor de los ", cantidadlapices, "lapices es de", cantidadtotal, "pesos")

elif cantidadlapices > 0 & cantidadlapices < 1000:
    cantidadtotal = cantidadlapices * 90 
    print ("EL valor de los ", cantidadlapices, "lapices es de", cantidadtotal, "pesos") 

else :
    print ("La cantidad de lápices no puede ser cero o una cifra negativa, intente nuevamente.")
